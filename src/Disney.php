<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
		//Result will be what is returned by the function.
        $result = array();
		
		//This array contains the name of actor
		$actorRes = array();
		
		//This array has information about all movies an actor has played in
		$starred = array();
		
		//This iterator is used to give every actor a separate array which 
		//keeps tracks of which movie they have played in. It's connected to starred. 
		$iterator = 0;



		//Query a xpath to all actors
		$node = $this->xpath->query('/Disney/Actors/Actor');
		$actors = $node;

		//Go through each actor
		foreach ($actors as $actor)
		{
			//Array of movies in which actor has played
			$starred[$iterator] = array();
			
			$actorName = $actor->childNodes[1]->nodeValue;
			$actorID = $actor->getAttribute("id");

			//Look for movies in which the actors played
			$node = $this->xpath->query('//Disney/Subsidiaries/Subsidiary/Movie');
			$movies = $node;

			//Go through each movie
			foreach ($movies as $movie){				
				$movieName = $movie->childNodes[1]->nodeValue;
				$movieYear = $movie->childNodes[3]->nodeValue;

				//Go through each role in every movie
				$roles = $this->xpath->query("//Disney/Subsidiaries/Subsidiary/Movie[Name = \"$movieName\" and Year = \"$movieYear\"]/Cast/Role");
				
				foreach ($roles as $role) {
					//If actor played in this movie
					if ($role->getAttribute("actor") == $actorID) {
						
						//Add him to starred[iterator] array 
						array_push($starred[$iterator], ("As " . $role->getAttribute("name") . " in " . $movieName . " (" . $movieYear . ")"));
					}
				}

			}
			
			//Prepare starred aray for next actor
			$iterator += 1;

			//Add name of actor to ActorRes
			array_push($actorRes, $actorName);

		}
		
		//Combine them together into result array
		$result = array_combine($actorRes, $starred);

		return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
	
      $actorNames = array();
      
	  //Keeps track of ID's of actors
	  $keys = array();
	  
	  //Query all the actors...
      $node = $this->xpath->query('/Disney/Actors/Actor');
      $actors = $node;
	  
	  //...and find their id
      foreach ($actors as $actor) {

			$actorID = $actor->getAttribute("id");
            array_push($keys, $actorID);

      }

		//Fill ActorNames with id's of actors and int 0 to check if they have played in a movie.
  		$actorNames = array_fill_keys($keys, 0);

		//Query movie...
      $node = $this->xpath->query('//Disney/Subsidiaries/Subsidiary/Movie');
      $movies = $node;

	  //...and go through every movie
      foreach ($movies as $movie) {

		//Look for cast in each movie
        $starred = $this->xpath->query("//Disney/Subsidiaries/Subsidiary/Movie/Cast/Role");

		//Go through every actor
        foreach ($actorNames as $actor) {
			
			//And go through every cast in a movie
          foreach ($starred as $star) {

			//If an actor was a cast in at least one of the movies
            if (key($actorNames) == $star->getAttribute("actor")) {
				
				//Increase his check integer by one.
              $actorNames[key($actorNames)] += 1;

            }

          }
			next($actorNames);
        }

		reset($actorNames);

		//Go through every actor
        foreach ($actorNames as $actor) {

		
			//If there is actor that did not play in a movie, delete him
			if ($actor == 0) {
				$idToDelete = key($actorNames);
			
				$nodeToDelete = $this->xpath->query("//Disney/Actors/Actor[@id = \"$idToDelete\"]");

				$nodeToDelete[0]->parentNode->removeChild($nodeToDelete[0]);

				$actorNames[key($actorNames)] += 1;
			  }
			next($actorNames);
        }

      }
	
    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
		$role = $this->doc->createElement('Role');
		$castNode = $this->xpath->query("//Disney/Subsidiaries/Subsidiary[@id = \"$subsidiaryId\"]/Movie[Name = \"$movieName\" and Year = \"$movieYear\"]/Cast")[0];
		$castNode->appendChild($role);
		$role->setAttribute('actor', $roleActor);
		$role->setAttribute('name', $roleName);
		if ($roleAlias != null) {
		$role->setAttribute('alias', $roleAlias);
		}
    }
}
?>
